export function useTableAction(tableData) {
  const removeRow = (rowIndex) => {
    tableData.splice(rowIndex, 1);
  };

  const addRowToTable = () => {
    tableData.push({
      uuid: crypto.randomUUID(),
      fullName: "",
      birthday: "",
      favoriteBook: "",
      sex: "",
      skills: [],
    });
  };

  return {
    removeRow,
    addRowToTable,
  };
}
